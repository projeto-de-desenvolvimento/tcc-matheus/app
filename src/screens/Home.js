import React, { Component } from 'react'
import { View, Text, ImageBackground, StyleSheet } from 'react-native'

import todayImage from '../../assets/imgs/today.jpg'
import moment from 'moment'
import 'moment/locale/pt-br'
import commonStyles from '../commonStyles.js'
import Object from '../components/Object'
import { FlatList } from 'react-native-gesture-handler'

export default class Home extends Component {

    state = {
        object: [{
            id: Math.random(),
            desc: 'Comprar Diesel',
            estimateAt: new Date(),
            doneAt: new Date()
        }, {
            id: Math.random(),
            desc: 'Pagar Funcionários',
            estimateAt: new Date(),
            doneAt: new Date()
        }]
    }

    toggleObject = objId => {
        const objects = [...this.state.objects]
        objects.forEach(object => {
            if (object.id === objId) {
                object.doneAt = object.doneAt ? null : new Date()
            }
        })

        this.setState({ object })

    }

    render() {
        const today = moment().locale('pt-br').format('ddd, D [de] MMMM')
        return (
            <View style={styles.container}>
                <ImageBackground source={todayImage} style={styles.background}>
                    <View style={styles.titleBar}>
                        <Text style={styles.title}>Bem-Vindo</Text>
                        <Text style={styles.subtitle}>{today}</Text>
                    </View>

                </ImageBackground>
                <View style={styles.home}>
                    <FlatList data={this.state.object} keyExtractor={item => `${item.id}`} renderItem={({ item }) => <Object {...item}
                        toggleObject={this.toggleObject} />} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    background: {
        flex: 3
    },
    home: {
        flex: 7
    },
    titleBar: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    title: {
        fontFamily: commonStyles.fontFamily,
        fontSize: 50,
        color: commonStyles.colors.secondary,
        marginLeft: 20,
        marginBottom: 20
    },
    subtitle: {
        fontFamily: commonStyles.fontFamily,
        fontSize: 20,
        color: commonStyles.colors.secondary,
        marginLeft: 20,
        marginBottom: 30
    }
})